package hackerranksolutions;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class SolutionTester {
    
    private final InputStream oldIn = System.in;
    private final PrintStream oldOut = System.out;
    private final PrintStream oldErr = System.err;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    
    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }
    
    @After
    public void tearDown() {
        System.setIn(oldIn);
        System.setOut(oldOut);
        System.setErr(oldErr);
    }
    
    private void setInContent(String content) {
        System.setIn(new ByteArrayInputStream(content.getBytes()));
    }

    public void assertInputEqualsOutput(
            String input, String expectedOutput, String solutionName) {
        try {
            setInContent(input);
            Class<?> solution = Class.forName(solutionName);
            Method method = solution.getMethod("main", String[].class);
            method.invoke(null, (Object) null);
            String actualOutput = outContent.toString();
            assertEquals(expectedOutput, actualOutput);
        } catch (NoSuchMethodException | IllegalAccessException | 
                InvocationTargetException | ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Test
    public void testHelloWorld() {
        String input = "world";
        String expectedOutput = "Hello, world!";
        String solutionName = "HelloWorld";
        assertInputEqualsOutput(input, expectedOutput, solutionName);
    }
}
